import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HousesComponent } from './houses/houses.component';
import { LoginComponent } from './login/login.component';
import { CustomersComponent } from './customers/customers.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ModelComponent } from './model/model.component';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import { ChangeComponent } from './change/change.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'houses', component: HousesComponent },
  { path: 'customers', component: CustomersComponent },
  { path: 'model', component: ModelComponent },
  { path: 'customer-form', component: CustomerFormComponent },
  { path: 'change', component: ChangeComponent }

  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
