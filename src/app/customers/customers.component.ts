import { Router } from '@angular/router';
import { PredictService } from '../predict.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';


@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css'],

})
export class CustomersComponent implements OnInit {

  name: string;
  completed: boolean;
  
 phone:number;

 restocheck;


 

  userId;

 
  customers:Customer[];
  customers$;
  addCustomerFormOpen=false;
  rowToEdit:number = -1; 
  customerToEdit:Customer = {name:null, phone:null};

 
  /*sendName(name){
    this.customersService.getName(name);
    this.router.navigate(['/model']);

  }*/



  moveToEditState(index){
    console.log(this.customers[index].name);
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.phone = this.customers[index].phone;
    this.customerToEdit.country = this.customers[index].country;
    this.customerToEdit.population = this.customers[index].population;
    this.customerToEdit.currencies = this.customers[index].currencies;
    this.customerToEdit.languages = this.customers[index].languages;


    this.rowToEdit = index; 
  }
  
  updateCustomer(){
    let id = this.customers[this.rowToEdit].id;
    this.customersService.updateCustomer(this.userId,id, this.customerToEdit.name,this.customerToEdit.phone,this.customerToEdit.country,this.customerToEdit.population,this.customerToEdit.currencies,this.customerToEdit.languages);
    this.rowToEdit = null;
  }

 
  deleteCustomer(index){
    let id = this.customers[index].id;
    this.customersService.deleteCustomer(this.userId, id);
  }

  updateResult(index){
    this.customers[index].saved = true; 
    console.log(this.userId);
    console.log(this.customers[index].id)
    this.customersService.updateRsult(this.userId,this.customers[index].id,this.customers[index].result);
  }


  goToModel(index){
    console.log(index);
    this.customersService.getName(this.customers[index].id,this.customers[index].name,this.customers[index].currencies);
    this.router.navigate(['/model']);


  }
  displayedColumns: string[] = ['name', 'phone','country','population','currencies','languages','Delete', 'Edit', 'Predict', 'Result'];
 
  constructor(private customersService:CustomersService,
    private authService:AuthService,
    private predictionService:PredictService,
    private router:Router ) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.customers$ = this.customersService.getCustomers(this.userId);
          this.customers$.subscribe(
            docs => {         
              this.customers = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const customer:Customer = document.payload.doc.data();
                if(customer.result){
                  customer.saved = true; 
                }
                customer.id = document.payload.doc.id;
                   this.customers.push(customer); 
              }                        
            }
          )
      })
  }   
}
