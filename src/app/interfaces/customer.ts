import { Country } from './country';
export interface Customer {
    id?:string,
    name?:string | null,
    phone?:number,
    result?,
    saved?
    agent?:string,
    population?,
    timezones?,
    currencies?,
    languages?,
    country?

}
