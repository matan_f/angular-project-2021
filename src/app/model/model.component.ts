import { ChangeService } from './../change.service';
import { PredictService } from './../predict.service';

import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { AuthService } from '../auth.service';

import { Router } from '@angular/router';
import { NodeWithI18n } from '@angular/compiler';
import { Customer } from '../interfaces/customer';
import { CustomersService } from '../customers.service';
import {FormControl, Validators} from '@angular/forms';


@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.css']
})
export class ModelComponent implements OnInit {

  
  animalControl = new FormControl('', Validators.required);
  selectFormControl = new FormControl('', Validators.required);
  options = [
    {name: 'Yes' ,sound:'Saved'},
    {name: 'No',sound:'Saved'},
    
  ];
  

  
  

  @Output() update = new EventEmitter<Customer>()
  @Output() closeEdit = new EventEmitter<null>()
 
  
  customers$;  
  
  prediction:string;
  showDrop:boolean=false;
  userId:string;
  selected=false;
  
  email;
  predDrop:string;
  
  age;
  sequence;
  income;
  surtys;
  equity;
  insurance;
  mortgage;
  incomeDollar;
 info=[];
  price;
  dolar="USD";
  changes$;
  ans;
  pressed:boolean=false;
  option;
   
  str;
id=this.info[0];

 tostring(){
   if(this.option['name']=="Yes"){
     this.insurance=1;
   }else{
     this.insurance=0;
   }
  var list=[this.age,this.sequence,this.ans,this.surtys,this.equity,this.insurance,this.mortgage];
  
  this.str=""
  let i
  for (i=0;i<7;i++){
    this.str=this.str+list[i];
    this.str=this.str+",";
  }
  this.str=this.str.substr(0, this.str.length-1);
  return this.str;
  
 }


  predict(){
    console.log(this.option['name'])
    console.log(this.str)
    this.predictService.predict(this.str).subscribe(
      res => {
        console.log(res);
      if(res=='Yes'){
        this.predDrop='Yes'
      }else{
        this.predDrop='No'
      }return this.predDrop;
     }
      )
  
      }
  
     
    
      
  
     
  onSubmit(){}
  
  save(){
    this.customersService.updateResult(this.userId, this.id, this.predDrop);
    this.router.navigate(['/customers']);

  }
  change(){
    this.pressed=true;
    console.log(this.income);
    this.changes$ = this.changeService.getChange(this.info[2],this.dolar);
    let cuu = this.info[2]+"_"+this.dolar;
    this.changes$.subscribe(
      data => {
        this.ans = ((data[cuu])*this.income).toFixed(2);
        console.log(this.ans);
      })}

  
    constructor(public predictService:PredictService,public authService:AuthService,private router:Router,private customersService:CustomersService, private changeService:ChangeService) { }
  
    ngOnInit(): void {
      this.info=this.customersService.nameToService();
      this.id=this.info[0];
      console.log(this.id);
      this.authService.getUser().subscribe(
        user => {
          this.userId = user.uid;
          this.email=user.email;
          this.customers$ = this.customersService.getCustomers(this.userId);
          
      })}
  
  
  
    

}
